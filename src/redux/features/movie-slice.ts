import { createSlice, PayloadAction} from '@reduxjs/toolkit'

type InitialState = {
    value : MovieState;
}

type MovieState = {
    url:string,
    movieName:string,
}

const initialState = {
    value:{
        // movieId:"",
        url:"/discover/movie",
        movieName:"",
    } as MovieState,
} as InitialState;

export const movie= createSlice({
    name:"movie",
    initialState,
    reducers:{
        clearOut: () => {
            return initialState;
        },
        searchMovie: (state,action:PayloadAction<{url:string,movieName:string}>) =>{
            return{
                value:action.payload,
            }
        },
    }
});

export const {clearOut,searchMovie} = movie.actions;
export default movie.reducer;