"use client"
import './input.css'
import Navbar from "@/components/Navbar";
import Maincontent from '@/components/Maincontent';
import Footer from '@/components/footer';
import Display from '@/components/display';
import { useState, useEffect } from 'react';
import { useAppSelector } from '@/redux/store';

export default function Home() {

  const url = useAppSelector((state) => state.movieReducer.value.url)
  const movieNameData = useAppSelector((state) => state.movieReducer.value.movieName)
  useEffect(() => {
  }, [movieNameData])

  return (
    <>
     <div className='bg-slate-800'>
        <Navbar />
        <Maincontent url={url} movieName={movieNameData} />
      </div>
      <footer>
        <Footer />
      </footer>
    </>
  )
}
