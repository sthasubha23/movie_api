// 'use client'

// import { API } from "./config";
// import React,{useState,useEffect} from 'react'
// interface Movie {
//     title: string;
//   }
// export default function apifunctions() {
//   const Api_key = 'e72792d0ab0ba8df1d737a866c2f58dc';
//   const [movieData, setMovieData] = useState<Movie[] | null>(null);
//   const [error, setError] = useState<string | null>(null);
//   const d = '/discover/movie';
//     useEffect(() => {
//     const fetchData = async (filter:string)=>{
//         try {
//             const response = await fetch(`${API}${filter}?api_key=${Api_key}`);
//             console.log(response);
//             if(!response.ok){
//                 throw new Error(`HTTP error!:Status:${response.status}`)
//             }
//             const data = await response.json();
//             console.log(data.results)
//             // return data.results;
//             setMovieData(data.results);
            
//         } catch (error:any) {
//             setError(error.message);
//         }
//     }
    
//     fetchData(d);
//   }, []);
//   return (
//    <>
//       <div>
//         {error && <p>Error: {error}</p>}
//         {movieData && (
//           <div>
//             {movieData.map((movie: Movie, index: number) => (
//               <h1 key={index}>{movie.title}</h1>
//             ))}
//           </div>
//         )}
//       </div>
//    </>
//   )
// }
