'use client'
import React, { useState } from 'react'
import {clearOut, searchMovie} from '@/redux/features/movie-slice'
import { useDispatch } from 'react-redux'
import { AppDispatch } from '@/redux/store'
function Search() {

  const dispatch =  useDispatch<AppDispatch>();
  const [searchData, setSearchData] = useState("");
  
  const handleSearchClick = () => {
    dispatch(searchMovie({url:'/search/movie',movieName:searchData}));
    console.log('Search clicked', searchData);
  };
  return (
    <>
      <div className='rounded-full flex  h-1/3'>
        <input
          onChange={(e) => setSearchData(e.target.value)}
          type="search"
          className="inline-flex text-sm py-3 w-full bg-gray-700 text-white rounded-l-full pl-4 focus:outline-none focus:bg-gray-700 focus:text-gray-900"
        />
        <button
          className="px-4 py-1 bg-gray-700 text-slate-500 rounded-r-full  focus:outline-none focus:bg-black focus:text-white"
          onClick={handleSearchClick}
        >
          Search
        </button>
      </div>
    </>
  )
}

export default Search