import Link from 'next/link';
import React, { useState, useEffect } from 'react';
import Search from './Search';
import fetchData from '@/hooks/fetchData';
import { clearOut, searchMovie } from '@/redux/features/movie-slice';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '@/redux/store';

const Navbar = () => {
  const dispatch = useDispatch<AppDispatch>();
  const [movieList, setMovieList] = useState<any>([]);
  const [activeRoute, setActiveRoute] = useState('/movie/latest');

  const handleButtonClick = (label: string) => {
    dispatch(searchMovie({ url: label, movieName: '' }));
    console.log(label);
  };

  useEffect(() => {
    const fetchDataAndSetState = async () => {
      try {
        const data = await fetchData(``, `page=1`);
        if (data && data.length > 0) {
          setMovieList(data);
        } else {
          console.error('No data found');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchDataAndSetState();
    return () => { };
  }, [fetchData]);

  return (
    <div className='container mx-auto pt-1'>
        <nav className="flex flex-col lg:flex-row justify-between text-blue-50">
      <div className="flex flex-col lg:flex-row lg:items-center gap-4">
        <img src='/img/logo.jpg' alt="Logo" className="rounded-full h-12 lg:h-10 lg:w-15 lg:mr-5 " />
        <div className="flex flex-col lg:flex-row lg:gap-4 ">
          <section className="flex flex-wrap gap-4 py-2">
            <button className="btn-nav" onClick={() => handleButtonClick('/movie/latest')}>
              Latest
            </button>
            <button className="btn-nav" onClick={() => handleButtonClick('/trending/movie')}>
              Trending
            </button>
            <button className="btn-nav" onClick={() => handleButtonClick('/discover/movie')}>
              Movie
            </button>
            <button className="btn-nav" onClick={() => handleButtonClick('/discover/tv')}>
              <span>TV</span>&nbsp;<span>Shows</span>
            </button>
          </section>
        </div>
      </div>
      <Search />
    </nav>
    </div>
  );
};

export default Navbar;
