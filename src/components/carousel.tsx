// Import necessary modules and components
import 'tailwindcss/tailwind.css';
import fetchData from '@/hooks/fetchData';
import React, { useState, useEffect } from 'react';

interface Movie {
  id: number;
  title: string;
  backdrop_path: string;
  popularity: string;
  vote_average: number;
  vote_count: number;
}

const Carousel = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [trendingData, setTrendingData] = useState<any>([]);
  const [time_window, setTime_window] = useState<string>('day');
  const [isDay, setIsDay] = useState<boolean>(true);
  const [idMovie, setIdMovie] = useState<any>("0");

  useEffect(() => {
    const fetchDataAndSetState = async () => {
      try {
        const data = await fetchData(`/trending/movie/${time_window}`, `page=1`);
        if (data && data.length > 0) {
          setTrendingData(data);
        } else {
          console.error('No data found');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    getMovieId();
    fetchDataAndSetState();
    return () => {};
  }, [fetchData, isDay, idMovie]);

  const images = trendingData.map((movie: Movie) => `https://image.tmdb.org/t/p/original${movie.backdrop_path}`);
  const movieId = trendingData.map((movie: Movie) => movie.id);
  const title = trendingData.map((movie: Movie) => movie.title);
  const popularity = trendingData.map((movie: Movie) => movie.popularity);
  const voteAverage = trendingData.map((movie: Movie) => movie.vote_average);
  const voteCount = trendingData.map((movie: Movie) => movie.vote_count);

  const nextSlide = () => {
    setCurrentIndex((prevIndex) => (prevIndex + 1) % images.length);
  };
  const prevSlide = () => {
    setCurrentIndex((prevIndex) => (prevIndex === 0 ? images.length - 1 : prevIndex - 1));
  };

  const toogleDay = () => {
    if (isDay) {
      setIsDay(false);
      setTime_window('week');
    } else {
      setIsDay(true);
      setTime_window('day');
    }
  };

  const getMovieId = () => {
    const id = movieId[currentIndex];
    setIdMovie(id);
  };

  return (
    <>
      <div className='pt-4'>
        <div className="flex space-x-2 items-center justify-between py-2 ">
          <div className="flex items-center">
            <p className="text-2xl font-thin text-gray-600">
              Trending {isDay ? <span className="">Today</span> : <span> This Week</span>}
            </p>
          </div>
          <button
            onClick={toogleDay}
            className={`px-3 py-2 text-sm font-medium rounded-md focus:outline-none transition duration-300 bg-gray-800 text-gray-100
              ${
                isDay
                  ? 'hover:bg-gray-900 focus:border-blue-300 focus:ring focus:ring-blue-200 active:bg-gray-200'
                  : 'hover:bg-gray-900 focus:border-blue-300 focus:ring focus:ring-blue-200 active:bg-gray-200'
              }`}
          >
            {isDay ? 'Switch to Week' : 'Switch to Day'}
          </button>
        </div>
      </div>
      <div className="relative bg-black h-screen">
        <div className="relative h-full sm:flex sm:items-center sm:justify-center">
          <div className="bg-cover w-full h-full aspect-w-16 aspect-h-9">
            <img
              className="object-cover w-full h-full"
              src={images[currentIndex]}
              alt={`Backdrop for ${title[currentIndex]}`}
            />
            <div className="absolute inset-0 bg-gradient-to-t from-black to-transparent"></div>
            <div className="absolute inset-0 flex flex-col items-center justify-center">
              <h2 className="text-2xl md:text-4xl lg:text-6xl font-bold text-white">{title[currentIndex]}</h2>
              <p className="text-base md:text-lg lg:text-xl text-white">Popularity: {popularity[currentIndex]}</p>
              <p className="text-base md:text-lg lg:text-xl text-white">Vote Average: {voteAverage[currentIndex]}</p>
              <p className="text-base md:text-lg lg:text-xl text-white">Vote Count: {voteCount[currentIndex]}</p>
              <a href="#">
                <button
                  className="mt-4 bg-blue-700 hover:bg-blue-500 text-white font-semibold py-2 px-4 rounded-full"
                  onClick={getMovieId}
                >
                  <p className="text-lg text-white hidden">{movieId[currentIndex]}</p>
                  <span className="text-sm">View Details</span>
                </button>
              </a>
            </div>
          </div>
        </div>
        <div className="absolute top-1/2 left-0 transform -translate-y-1/2 h-full">
          <button
            className="absolute left-0 top-1/2 transform -translate-y-1/2 px-4 py-2 h-full bg-transparent text-gray-50/[0.1] text-4xl md:text-6xl lg:text-8xl"
            onClick={prevSlide}
          >
            &lt;
          </button>
        </div>

        <div className="absolute top-1/2 right-0 transform -translate-y-1/2 h-full">
          <button
            className="absolute right-0 top-1/2 transform -translate-y-1/2 px-4 py-2 h-full bg-transparent text-gray-50/[0.1] text-4xl md:text-6xl lg:text-8xl"
            onClick={nextSlide}
          >
            &gt;
          </button>
        </div>
      </div>
    </>
  );
};

export default Carousel;
