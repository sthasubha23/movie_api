import React from 'react';

const Card = ({ imageUrl, title, description, rating, movieId }: any) => {
  return (
    <>
      <a className='' id={movieId}>
        <div className="relative group">
          <div className="bg-cover aspect-auto h-96 group-hover:bg-opacity-50 transition-opacity" style={{ backgroundImage: `url('https://image.tmdb.org/t/p/original/${imageUrl}')` }}>
            <div className="absolute top-0 right-0 p-4">
              <p className="text-white text-sm bg-gray-800 px-2 py-1">{rating}/10.0</p>
            </div>
            <div className="absolute w-full bottom-0 left-0 py-3 bg-slate-900/[0.9]">
              <h6 className="text-bold text-white text-center">
                <span className=" overflow-hidden overflow-ellipsis">
                  {title}
                </span>
              </h6>
            </div>
          </div>
        </div>
      </a>
    </>
  );
};

export default Card;