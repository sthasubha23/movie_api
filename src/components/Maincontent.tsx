'use client'
import React, { useState, useEffect } from 'react';
import fetchDataUrl from '@/hooks/fetchDataUrl';
import Carousel from './carousel';
import Card from './card';
import Display from './display';
import { ApiResponse, Movie } from '../app/interface/interface'

function Maincontent({ url, movieName }: any) {
  const [pageCount, setPageCount] = useState<number>(1);
  const [movieData, setMovieData] = useState<any[]>([]);
  const [displayMovie, setDisplayMovie] = useState<any>(null);

  const handleCloseDisplay = () => {
    setDisplayMovie(null);
  };

  const handleNext = () => {
    setPageCount(pageCount + 1)
    console.log(pageCount)
  }
  const handlePrev = () => {
    if (pageCount > 1) {
      console.log("prev")
      setPageCount(pageCount - 1)
    }
    else {
    }
  }

  const handleDisplay = (movieId: any) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setDisplayMovie(movieId);
  }

  useEffect(() => {
    const fetchDataAndSetState = async () => {
      try {
        let query;
        if (pageCount) {
          query = `${movieName}&page=${pageCount}`
          console.log(query)
        }
        const data: ApiResponse = await fetchDataUrl(`${url}`, `${query}`);
        console.log(data)
        if (data.results && data.results.length > 0) {
          setMovieData(data.results);
        } else {
          console.error('No data found');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchDataAndSetState();
    return () => {
    }
  }, [pageCount, fetchDataUrl, url, movieName])

  return (
    <>
      <div className='container mx-auto'>
        {displayMovie && <Display movieId={displayMovie} onClose={handleCloseDisplay} />}
        {movieData && (
          <>
            <div className='mb-12'>
              <Carousel />
            </div>
            <p className='text-xl md:text-4xl lg:text-4xl font-bold mb-2 text-white'>
              {url === "/discover/movie" ? ("Discover") :
                url === "/discover/tv" ? ("TV Show") :
                  url === "/movie/latest" ? ("Latest") :
                    url === "/trending/movie" ? ("Trending") :
                      ('')}</p>
            <div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8'>
              {movieData.map((movie: Movie, index: number) => (
                <button key={index} className='mb-4' onClick={() => handleDisplay(movie.id)}>
                  <Card imageUrl={movie.poster_path} title={movie.title} description={movie.overview} rating={movie.vote_average} movieId={movie.id} />
                </button>
              ))}
            </div>
          </>

        )}
        <nav className="flex items-center justify-between px-4 py-3 border-gray-200 rounded-full" aria-label="Pagination">
          <div className="flex items-center">
            <p className="text-sm text-gray-600">
              Showing page <span className="font-medium">{pageCount}</span> with <span className="font-medium">20</span> results
            </p>
          </div>
          <div className="flex space-x-2">
            <button
              onClick={handlePrev}
              className="px-3 py-2 text-sm font-medium text-gray-100 bg-slate-900 border border-gray-300 rounded-md hover:bg-gray-50 focus:outline-none focus:border-blue-300 focus:ring focus:ring-blue-200 active:bg-gray-200"
            >
              Previous
            </button>
            <button
              onClick={handleNext}
              className="px-3 py-2 text-sm font-medium text-gray-100 bg-slate-900 border border-gray-300 rounded-md hover:bg-gray-50 focus:outline-none focus:border-blue-300 focus:ring focus:ring-blue-200 active:bg-gray-200"
            >
              Next
            </button>
          </div>
        </nav>
      </div>
    </>
  )
}

export default Maincontent