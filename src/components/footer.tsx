import React from "react";
import {
  FaFacebookF,
  FaInstagram,
  FaTwitter,
  FaLinkedin,
} from "react-icons/fa";

const Footer: React.FC = () => {
  return (
    <footer className="bg-gray-900 text-gray-300 py-12 px-4">
    <div className="container mx-auto">
      <div className="flex flex-wrap justify-between">
        <div className="w-full md:w-1/2 md:pr-8 mb-8 md:mb-0">
          <h6 className="text-xl font-bold mb-4">About</h6>
          <p className="text-justify">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </p>
        </div>

        <div className="w-full md:w-1/4">
          <h6 className="text-xl font-bold mb-4">Tools used</h6>
          <ul className="footer-links">
            <li><a href="#">Next JS</a></li>
            <li><a href="#">TMDB</a></li>
            <li><a href="#">RESTful APIs</a></li>
            <li><a href="#">JavaScript</a></li>
            <li><a href="#">React</a></li>
            <li><a href="#">Hooks</a></li>
          </ul>
        </div>

        <div className="w-full md:w-1/4">
          <h6 className="text-xl font-bold mb-4">Quick Links</h6>
          <ul className="footer-links">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Contribute</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Sitemap</a></li>
          </ul>
        </div>
      </div>

      <hr className="border-gray-700 my-8" />

      <div className="flex items-center justify-between">
        <div className="md:w-2/3">
          <p className="text-sm">&copy; 2017 All Rights Reserved by <a href="#">Movie_api</a>.</p>
        </div>

        <div className="md:w-1/3 text-right">
          <ul className="social-icons">
            <li><a className="facebook" href="#"><i className="fa fa-facebook"></i></a></li>
            <li><a className="twitter" href="#"><i className="fa fa-twitter"></i></a></li>
            <li><a className="dribbble" href="#"><i className="fa fa-dribbble"></i></a></li>
            <li><a className="linkedin" href="#"><i className="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  );
};

export default Footer;
