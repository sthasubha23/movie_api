'use client'
import fetchSingleData from '../hooks/fetchDataUrl'
import React, { useEffect, useState } from 'react'

function display({ movieId, onClose }: any) {
  const [movieDetails, setMovieDetails] = useState<any>();
  const [isOpen, setIsOpen] = useState(true);

  useEffect(() => {
    const fetchDataAndSetState = async () => {
      try {
        const data = await fetchSingleData(`/movie/${movieId}`, '');
        console.log(data)
        if (data) {
          setMovieDetails(data);
        } else {
          console.error('No data found');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchDataAndSetState();
    return () => {
    }
  }, [fetchSingleData, movieId])

  const handleClose = () => {
    setIsOpen(false);
    onClose();
  };

  const title = movieDetails ? movieDetails.original_title : 'N/A';
  const popularity = movieDetails ? movieDetails.popularity : 'N/A';
  const voteAverage = movieDetails ? movieDetails.vote_average : 'N/A';
  const voteCount = movieDetails ? movieDetails.vote_count : 'N/A';
  const images = movieDetails && movieDetails.poster_path ? `https://image.tmdb.org/t/p/original${movieDetails.poster_path}` : 'N/A';
  const release_date = movieDetails?.release_date;
  const overview = movieDetails?.overview;
  const genres = movieDetails && movieDetails.genres
    ? movieDetails.genres.map((genre: { name: string }) => genre.name)
    : [];
  return (
    <>
      {isOpen && (
        <div className="container relative mx-auto mt-2 ">
          <div className="relative flex flex-row gap-8 bg-cover w-full h-auto" style={{ backgroundImage: `url(${images})` }}>

            {/* bg_overlay*/}
            <div className="absolute inset-0 bg-gradient-to-t rounded-sm from-slate-800 from-10% via-slate-700 via-30% to-slate-900 to-90% backdrop-filter backdrop-blur-2xl bg-transparent"></div>

            {/* detail_section*/}
            <div className="relative flex justify-between p-8">

              {/* image*/}
              <div className='w-4/12'>
                <img className="w-full rounded-xl" src={images} alt="Not Found" />
              </div>

              {/* details */}
              <div className="mx-8 w-8/12">
                <div className="mt-4">
                  <h1 className="text-4xl font-bold text-white">{title}</h1>
                  <p className="text-gray-100">{overview}</p>
                </div>
                <div className="flex flex-col mt-4">
                  <div className='flex flex-row space-x-2'>
                    {movieDetails && movieDetails.genres
                      ? movieDetails.genres.map((genre: { name: string }) => (
                        <span key={genre.name} className="text-lg font-sans font-bold text-white">
                          #{genre.name.toUpperCase()}&nbsp;
                        </span>
                      ))
                      : null}
                  </div>
                  <h1 className="text-lg text-white mt-2">Release Date: {release_date}</h1>
                  <h1 className="text-lg text-white">Average Vote: {voteAverage} / 10.0</h1>
                  <h1 className="text-lg text-white">Popularity: {popularity}</h1>
                  <h1 className="text-lg text-white">Vote Count: {voteCount}</h1>
                </div>
              </div>
            </div>
          </div>
          <button className="absolute top-4 right-4 text-white cursor-pointer rounded" onClick={handleClose}>
            <span className="material-symbols-outlined">
              close
            </span>
          </button>
        </div>
      )}
    </>
  )
}

export default display