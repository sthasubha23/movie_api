import { API,Api_key } from "../app/api/config";
export default async function fetchData(filter: string,queries:string): Promise<any[]> {
    try {
      const response = await fetch(`${API}${filter}?api_key=${Api_key}&${queries}`);
      // console.log(response);
      
      if (!response.ok) {
        throw new Error(`HTTP error!: Status: ${response.status}`);
      }
      
      const data = await response.json();
    //   console.log(data.results);
  
      return data.results;
    } catch (error: any) {
        // console.log(error,'here is the error')
      return [error.message];
    }
  }

  // https://api.themoviedb.org/3/search/movie
  