import { API,Api_key } from "../app/api/config";
import {ApiResponse} from '../app/interface/interface'

export default async function fetchData(url: string,query:string): Promise<ApiResponse> {
    try {
        const response = await fetch(`${API}${url}?query=${query}&api_key=${Api_key}`);
        console.log(`${API}${url}?query=${query}&api_key=${Api_key}`);

      
      if (!response.ok) {
        throw new Error(`HTTP error!: Status: ${response.status}`);
      }
      
      const data = await response.json();
      console.log(data);

      return data;
    } catch (error:any) {
      console.log(error.message)
      return error.message;
    }
  }